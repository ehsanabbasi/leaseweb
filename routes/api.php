<?php

use App\Http\Controllers\ServerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::group(['prefix' => 'v1'], function () {
    Route::post('excel/upload', [ServerController::class, 'uploadServersExcel'])->name('uploadServersExcel');
});*/

Route::prefix('v1/')
    ->name('api.v1.')
    ->as('api.v1.')
    ->group(function () {
        Route::prefix('servers')
            ->name('servers.')
            ->as('servers.')
            ->group(function (): void {
                Route::post('excel/upload', [ServerController::class, 'uploadServersExcel'])->name('uploadServersExcel');
                Route::get('/', [ServerController::class, 'getServers'])->name('getServers');
                Route::delete('/delete/{id}', [ServerController::class, 'deleteServer'])->name('deleteServer');
                Route::get('/delete', [ServerController::class, 'deleteAllServers'])->name('deleteAllServers');
                Route::get('/locations', [ServerController::class, 'getServersLocations'])->name('getServersLocations');
            });
    });
