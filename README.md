# Application Introduction

Story behind this application:
This application get your proper **Excel file** and save it to your database, then you can use these below filters to filter your servers :
- Filter by **hard disk storage Range**: (0, 250GB, 500GB, 1TB, 2TB, 3TB, 4TB, 8TB, 12TB, 24TB, 48TB, 72TB)
- Filter by multiple **RAM size**: 2GB, 4GB, 8GB, 12GB, 16GB, 24GB, 32GB, 48GB, 64GB, 96GB
- Filter by **hard disk type**: SAS, SATA, SSD
- Filter by **Location** which is in the Excel file

# Application setup
- Install Docker and docker-compose on your machine
- Clone Project files from gitlab (please tell me to add you to my repository) OR use zip file
- Go to project directory
- Run command **cp .env.example .env**
- Put this database configs in your env file:
    - DB_CONNECTION=mysql
    - DB_HOST=leaseweb-mysql
    - DB_PORT=3306
      **And Complete your database config whatever you want "DB_DATABASE, DB_USERNAME, DB_PASSWORD and DB_ROOT_PASSWORD"**
- Run command **docker-compose up  -d**
- Run command **docker exec leaseweb-php composer install**
- Run command **docker exec leaseweb-php php artisan key:generate**
- Run command **docker exec leaseweb-php php artisan migrate**
- Run command **docker exec leaseweb-php php artisan l5-swagger:generate**
- Check this 127.0.0.1:9010
- To run tests please run **docker exec leaseweb-php php artisan test**

# Api Documentation
- Please check this path on your machine **localhost:9010/api/documentation**
