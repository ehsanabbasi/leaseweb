<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class HelperTest extends TestCase
{
    public function GetStringFirstNumbersProvider(): array
    {
        return [
            ['128GBDDR4', '128'],
            ['32GBDDR3', '32'],
            ['16GBDDR4', '16'],
            ['4GBDDR3', '4']
        ];
    }

    /**
     * @dataProvider GetStringFirstNumbersProvider
     * @param string $string
     * @param string $expected
     */
    public function testGetStringFirstNumbers(string $string, string $expected): void
    {
        self::assertEquals($expected, getStringFirstNumbers($string));
    }

    public function IsTerabyteInStringProvider(): array
    {
        return [
            ['2x2TBSATA2', true],
            ['2x10TBSATA2', true],
        ];
    }

    /**
     * @dataProvider IsTerabyteInStringProvider
     * @param string $string
     */
    public function testIsTerabyteInString(string $string): void
    {
        self::assertTrue(isTerabyteInString($string));
    }

    public function isGigaByteInStringProvider(): array
    {
        return [
            ['2x2GBSATA2', true],
            ['2x10GBSSD', true],
        ];
    }

    /**
     * @dataProvider isGigaByteInStringProvider
     * @param string $string
     */
    public function testIsGigaByteInString(string $string): void
    {
        self::assertTrue(isGigaByteInString($string));
    }

    public function getStringAfterOneCharacterProvider(): array
    {
        return [
            ['2x2GBSATA2', 'B', 'SATA2'],
            ['2x2GBSATA2', 'x', '2GBSATA2'],
            ['2x10GBSSD', 'B', 'SSD'],
            ['2x10GBSSD', 'x', '10GBSSD'],
            ['16GBDDR4', 'B', 'DDR4'],
            ['4GBDDR3', 'B', 'DDR3'],
        ];
    }

    /**
     * @dataProvider getStringAfterOneCharacterProvider
     * @param string $string
     * @param string $needle
     * @param string $expected
     */
    public function testGetStringAfterOneCharacter(string $string, string $needle, string $expected): void
    {
        self::assertEquals($expected, getStringAfterOneCharacter($string, $needle));
    }

    public function decorateHardDiskStorageRangeProvider(): array
    {
        return [
            ['500GB,10TB', [500, 10000]],
            ['500GB,1TB', [500, 1000]],
            ['500GB,500GB', [500, 500]],
            ['50GB,15TB', [50, 15000]],
            ['500GB,1TB', [500, 1000]],
            ['250GB,72TB', [250, 72000]],
        ];
    }

    /**
     * @dataProvider decorateHardDiskStorageRangeProvider
     * @param string $string
     * @param array $expected
     */
    public function testDecorateHardDiskStorageRange(string $string, array $expected): void
    {
        self::assertEquals($expected, decorateHardDiskStorageRange($string));
    }

    public function decorateRamSizeAndRamTypeProvider(): array
    {
        return [
            ['16GBDDR4', ['ram_type' => 'DDR4' , 'ram_size' => '16GB']],
            ['4GBDDR3', ['ram_type' => 'DDR3' , 'ram_size' => '4GB']],
        ];
    }

    /**
     * @dataProvider decorateRamSizeAndRamTypeProvider
     * @param string $string
     * @param array $expected
     */
    public function testDecorateRamSizeAndRamType(string $string, array $expected): void
    {
        self::assertEquals($expected, decorateRamSizeAndRamType($string));
    }

    public function decorateHardDiskTypeAndStorageProvider(): array
    {
        return [
            ['2x2GBSATA2', ['hdd_type' => 'SATA2', 'hdd_storage' => '4']],
            ['2x250GBSATA2', ['hdd_type' => 'SATA2', 'hdd_storage' => '500']],
            ['2x10GBSSD', ['hdd_type' => 'SSD', 'hdd_storage' => '20']],
            ['2x10TBSSD', ['hdd_type' => 'SSD', 'hdd_storage' => '20000']],
        ];
    }

    /**
     * @dataProvider decorateHardDiskTypeAndStorageProvider
     * @param string $string
     * @param array $expected
     */
    public function testDecorateHardDiskTypeAndStorage(string $string, array $expected): void
    {
        self::assertEquals($expected, decorateHardDiskTypeAndStorage($string));
    }
}
