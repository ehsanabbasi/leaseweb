<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('model');
            $table->string('ram');
            $table->string('ram_size');
            $table->string('ram_type');
            $table->string('hdd');
            $table->integer('hdd_storage');
            $table->string('hdd_type');
            $table->string('location');
            $table->string('price');
            $table->index(['ram_size', 'hdd_storage', 'hdd_type', 'location']);
            $table->index(['ram_size']);
            $table->index(['hdd_storage']);
            $table->index(['hdd_type']);
            $table->index(['location']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
