<?php

namespace App\Documentation\Excel;

class UploadExcelDoc
{
    /**
     *
     * @OA\Post(
     *     path="/servers/excel/upload",
     *     tags={"Servers"},
     *     @OA\Parameter (
     *          name="file",
     *          description = "Choose your excel file to upload",
     *          in="path",
     *          required=true,
     *          ),
     *     @OA\Response(
     *          response="200",
     *          description="Excel file imported successfully",
     *              @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  example="Excel file imported successfully"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Wrong file format",
     *                  @OA\JsonContent(
     *                  @OA\Property(
     *                    property="error",
     *                    example="The file must be a file of type: xlsx, ods, odt, odp."
     *                  )
     *              )
     *      ),
     * )
     */
}
