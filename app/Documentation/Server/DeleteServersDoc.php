<?php

namespace App\Documentation\Server;

class DeleteServersDoc
{
    /**
     * * @OA\Get(
     *     path="/servers/delete",
     *     tags={"Servers"},
     *     summary="Delete all servers",
     *     description="Delete all servers (Please Consider that all your rows in database will be delete)",
     *     @OA\Response(
     *          response="200",
     *          description="Delete all servers",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  example="All servers deleted"
     *              ),
     *          )
     *      )
     * )
     *
     **/
}
