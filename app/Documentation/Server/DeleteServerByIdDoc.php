<?php

namespace App\Documentation\Server;

class DeleteServerByIdDoc
{
    /**
     * * @OA\Delete(
     *     path="/servers/delete/{id}",
     *     tags={"Servers"},
     *     summary="Delete server by id",
     *     description="Delete server by id",
     *     @OA\Parameter (
     *          name="id",
     *          description = "send your id to delete",
     *          in="path",
     *          required=true,
     *          ),
     *     @OA\Response(
     *          response="200",
     *          description="Server number:{id} deleted successfully",
     *     @OA\JsonContent(
     *              @OA\Property(
     *                  property="message",
     *                  example="Server number:{id} deleted successfully"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="No server found to delete",
     *              @OA\JsonContent(
     *              @OA\Property(
     *                  property="error",
     *                  example="No server found to delete"
     *              )
     *          )
     *      )
     * )
     */
}
