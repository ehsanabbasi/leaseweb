<?php

namespace App\Documentation\Server;

class ServersDoc
{
    /**
     * @OA\Get(
     *      path="/servers",
     *      operationId="getServers",
     *      tags={"Servers"},
     *      summary="Get list of servers",
     *      description="Returns list of servers",
     *     @OA\Parameter (
     *          name="ram",
     *          description = "Choose your Rams",
     *          in="path",
     *          example = "8GB, 12GB, 16GB",
     *          ),
     *     @OA\Parameter (
     *          name="hdd",
     *          description = "Choose your hard disk storage range",
     *          in="path",
     *          example = "500GB,10TB",
     *          ),
     *     @OA\Parameter (
     *          name="hard_disk_type",
     *          description = "Choose your hard disk type",
     *          in="path",
     *          example = "SSD",
     *          ),
     *     @OA\Parameter (
     *          name="location",
     *          description = "Choose your location",
     *          in="path",
     *          example = "Washington D.C.WDC-01",
     *          ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              allOf={
     *                  @OA\Schema(ref="#/components/schemas/PaginationDocResponse"),
     *                  @OA\Schema(
     *                  @OA\Property( type="array",property="data",
     *                      @OA\Items(
     *                          allOf={
     *                              @OA\Schema(ref="#/components/schemas/ServerModelResponseDoc")
     *                          }
     *                      )
     *                  )
     *              )
     *              }
     *          )
     *       ),
     *     )
     */
}
