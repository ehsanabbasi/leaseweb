<?php


namespace App\Documentation\Server;

/**
 * Class ServerModelResponseDoc
 * @OA\Schema(
 *     description="Server success response",
 *     title="Server success response"
 * )
 */
class ServerModelResponseDoc
{
    /**
     * @OA\Property(
     *     title="id",
     *     description="id",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="model",
     *     description="model name",
     *     example="HP DL120G6Intel Xeon X3440"
     * )
     *
     * @var string
     */
    private $model;

    /**
     * @OA\Property(
     *     title="ram",
     *     description="ram inforamtion",
     *     example="8GBDDR3"
     * )
     *
     * @var string
     */
    private $ram;

    /**
     * @OA\Property(
     *     title="hdd",
     *     description="hdd inforamtion",
     *     example="2x240GBSSD"
     * )
     *
     * @var string
     */
    private $hdd;

    /**
     * @OA\Property(
     *     title="locaiton",
     *     description="location inforamtion",
     *     example="Washington D.C.WDC-01"
     * )
     *
     * @var string
     */
    private $location;

    /**
     * @OA\Property(
     *     title="price",
     *     description="pricae inforamtion",
     *     example="$69.99"
     * )
     *
     * @var string
     */
    private $price;
}
