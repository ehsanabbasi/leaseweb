<?php

namespace App\Documentation\Server;

class ServersLocations
{
    /**
     * * @OA\Get(
     *     path="/servers/locations",
     *     tags={"Servers"},
     *     summary="Show all possible locations",
     *     description="Show all possible locations",
     *     @OA\Response(
     *          response="200",
     *          description="Show all location",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="locations",
     *                  type="array",
     *                  @OA\Items(
     *                    example="AmsterdamAMS-01"
     *                  ),
     *              ),
     *          )
     *      )
     * )
     *
     **/
}
