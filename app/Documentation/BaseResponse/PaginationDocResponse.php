<?php

namespace App\Documentation\BaseResponse;

/**
 * Class PaginationDocResponse
 * @OA\Schema(
 *     description="API pagnination response",
 *     title="API pagnination response"
 * )
 */
class PaginationDocResponse
{

    /**
     * @OA\Property(
     *     description="current_page",
     *     title="current_page",
     *     example=1,
     *     type="integer"
     * )
     *
     * @var string
     */
    private $current_page;

    /**
     * @OA\Property(
     *     description="Data",
     *     title="data",
     *     format="object"
     * )
     *
     * @var object
     */
    private $data;

    /**
     * @OA\Property( type="array",
     *   @OA\Items(
     *      @OA\Property(
     *         property="url",
     *         example=null,
     *         type="string"
     *         ),
     *      @OA\Property(
     *         property="label",
     *         example="« Previous",
     *         type="string"
     *         ),
     *      @OA\Property(
     *         property="active",
     *         example=false,
     *         type="boolean"
     *         ),
     *    )
     * )
     *
     * @var object
     */
    private $links;

    /**
     * @OA\Property(
     *     description="first_page_url",
     *     title="first_page_url",
     *     example="?page=1",
     *     type="string"
     * )
     *
     * @var string
     */
    private $first_page_url;

    /**
     * @OA\Property(
     *     description="from",
     *     title="from",
     *     example=1,
     *     type="integer"
     * )
     *
     * @var string
     */
    private $from;

    /**
     * @OA\Property(
     *     description="last_page",
     *     title="last_page",
     *     example=16,
     *     type="integer"
     * )
     *
     * @var string
     */
    private $last_page;

    /**
     * @OA\Property(
     *     description="last_page_url",
     *     title="last_page_url",
     *     example="?page=16",
     *     type="string"
     * )
     *
     * @var string
     */
    private $last_page_url;

    /**
     * @OA\Property(
     *     description="next_page_url",
     *     title="next_page_url",
     *     example="?page=2",
     *     type="string"
     * )
     *
     * @var string
     */
    private $next_page_url;

    /**
     * @OA\Property(
     *     description="current url",
     *     title="current url",
     *     example="/current-url",
     *     type="string"
     * )
     *
     * @var string
     */
    private $path;

    /**
     * @OA\Property(
     *     description="per page data",
     *     title="per_page",
     *     example=15,
     *     type="integer"
     * )
     *
     * @var string
     */
    private $per_page;

    /**
     * @OA\Property(
     *     description="prev_page_url",
     *     title="prev_page_url",
     *     example="null",
     *     type="string"
     * )
     *
     * @var string
     */
    private $prev_page_url;

    /**
     * @OA\Property(
     *     description="to",
     *     title="to",
     *     example=15,
     *     type="integer"
     * )
     *
     * @var string
     */
    private $to;

    /**
     * @OA\Property(
     *     description="total",
     *     title="total",
     *     example=240,
     *     type="integer"
     * )
     *
     * @var string
     */
    private $total;
}
