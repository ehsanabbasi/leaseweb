<?php

function getStringFirstNumbers(string $string) {
    preg_match("~^(\d+)~", $string, $matches);
    return $matches[0];
}

function isTerabyteInString($string) {
    return strpos($string, 'TB') !== false;
}

function isGigaByteInString($string) {
    return strpos($string, 'GB') !== false;
}

function getStringAfterOneCharacter($string, $needle) {
    return substr($string, strpos($string, $needle) + 1);
}

function decorateHardDiskStorageRange($hardDiskStorageRange) {
    $hardDiskStorageRange = trim($hardDiskStorageRange, ' ');
    $hardDiskStorageRange = explode(',', $hardDiskStorageRange);
    foreach ($hardDiskStorageRange as $key => $item) {
        preg_match("~^(\d+)~", $item, $multiplyNumber);
        if (strpos($item, 'TB') !== false) {
            $hardDiskStorageRange[$key] = (int)$multiplyNumber[0] * 1000;
        } elseif (strpos($item, 'GB') !== false) {
            $hardDiskStorageRange[$key] = (int)$multiplyNumber[0];
        }
    }
    return $hardDiskStorageRange;
}

function decorateRamSizeAndRamType($ramData): array {
    $ramType = getStringAfterOneCharacter($ramData, "B");
    return [
        'ram_type' => $ramType,
        'ram_size' => str_replace($ramType, '', $ramData)
    ];
}

function decorateHardDiskTypeAndStorage($HardDiskData): array {

    $hdd_count = getStringAfterOneCharacter($HardDiskData, "x");
    $hdd_storage = 0;
    if (isTerabyteInString($HardDiskData)) {
        $hdd_storage = getStringFirstNumbers($HardDiskData) * getStringFirstNumbers($hdd_count) * 1000;
    } elseif (isGigaByteInString($HardDiskData)) {
        $hdd_storage = getStringFirstNumbers($HardDiskData) * getStringFirstNumbers($hdd_count);
    }
    $hdd_type = getStringAfterOneCharacter($HardDiskData, "B");

    return [
        'hdd_type' => $hdd_type,
        'hdd_storage' => $hdd_storage
    ];
}
