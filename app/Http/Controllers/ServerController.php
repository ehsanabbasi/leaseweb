<?php

namespace App\Http\Controllers;

use App\Imports\ServersImport;
use App\Models\Server;
use App\Repositories\Filter\FilterRepository;
use Illuminate\Support\Facades\Redis;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ServerController extends Controller
{
    private $filterService;

    public function __construct(FilterRepository $filterService)
    {
        $this->filterService = $filterService;
    }

    public function uploadServersExcel(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(),
            [
                'file'=>'required|max:180|mimes:xlsx,xls,ods'
            ]
        );
        if ($validator->fails()) {
            return response()
                ->json(['error' => $validator->errors()->get('file')], 422);
        }

        Excel::import(new ServersImport(), $request->file('file'));
        return response()
            ->json(['message' => 'Excel file imported successfully']);
    }

    public function getServers(Request $request): JsonResponse
    {
        return response()
            ->json($this->filterService->getServersByFilters($request));
    }

    public function deleteServer($id): JsonResponse
    {
        if (Server::destroy($id)) {
            return response()
                ->json(['message' => 'Server number:' . $id .' deleted successfully']);
        }
        return response()
            ->json(['error' => 'No server found to delete'], 404);
    }

    public function deleteAllServers(): JsonResponse
    {
        Server::truncate();
        Redis::del('locations');
        return response()
            ->json(['message' => 'All servers deleted']);
    }

    public function getServersLocations(): JsonResponse
    {
        $locations = Redis::get('locations');

        if ($locations) {
            return response()
                    ->json(['locations' => json_decode($locations, true,)]);
        }

        $locations = Server::select('location')->distinct()->pluck('location');
        Redis::set('locations', json_encode($locations->toArray()));

        return response()
            ->json(['locations' => $locations]);
    }
}
