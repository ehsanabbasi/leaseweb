<?php

namespace App\Imports;

use App\Models\Server;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use phpDocumentor\Reflection\Types\Void_;

class ServersImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row): Server
    {
        $ram = decorateRamSizeAndRamType($row['ram']);
        $hdd = decorateHardDiskTypeAndStorage($row['hdd']);

        return new Server([
            'model'         => $row['model'],
            'ram'           => $row['ram'],
            'ram_size'      => $ram['ram_size'],
            'ram_type'      => $ram['ram_type'],
            'hdd'           => $row['hdd'],
            'hdd_type'      => $hdd['hdd_type'],
            'hdd_storage'   => $hdd['hdd_storage'],
            'location'      => $row['location'],
            'price'         => $row['price']
        ]);
    }
}
