<?php

namespace App\Repositories\Filter;

interface FilterRepository
{
    public function getServersByFilters($request);
}
