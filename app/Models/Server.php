<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'model',
        'ram',
        'ram_size',
        'ram_type',
        'hdd',
        'hdd_storage',
        'hdd_type',
        'location',
        'price',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'ram_size',
        'ram_type',
        'hdd_storage',
        'hdd_type',
    ];

}
