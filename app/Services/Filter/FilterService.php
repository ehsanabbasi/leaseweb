<?php

namespace App\Services\Filter;

use App\Models\Server;
use App\Repositories\Filter\FilterRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class FilterService implements FilterRepository
{
    public const DEFAULT_PAGINATE = 15;

    public function getServersByFilters($request): LengthAwarePaginator
    {
        $serverQuery = Server::query();

        if ($request->has('ram')) {
            $this->serversFilterByRAMSize($serverQuery, $request->get('ram'));
        }

        if ($request->has('hard_disk_storage_range')) {
            $hardDiskStorageRange = decorateHardDiskStorageRange($request->get('hard_disk_storage_range'));
            $serverQuery = $this->serversFilterByHardDiskStorageRange($serverQuery, $hardDiskStorageRange);
        }

        if ($request->has('hard_disk_type')) {
            $serverQuery = $this->serversFilterByHardDiskType($serverQuery, $request->get('hard_disk_type'));
        }

        if ($request->has('location')) {
            $serverQuery = $this->serversFilterByLocation($serverQuery, $request->get('location'));
        }

        return $serverQuery->paginate(self::DEFAULT_PAGINATE);
    }

    private function serversFilterByRAMSize($serverQuery, $ramSize)
    {
        $ramSize = trim($ramSize, ' ');
        $rams = explode(',', $ramSize);
        return $serverQuery->whereIn('ram_size', $rams);
    }

    private function serversFilterByHardDiskStorageRange($serverQuery, $hardDiskStorageRange)
    {
        return $serverQuery->whereBetween('hdd_storage', $hardDiskStorageRange);
    }

    private function serversFilterByHardDiskType($serverQuery, $hardDiskType)
    {
        return $serverQuery->where('hdd_type', 'like', '%' . $hardDiskType . '%');
    }

    private function serversFilterByLocation($serverQuery, $location)
    {
        return $serverQuery->where('location', $location);
    }
}
